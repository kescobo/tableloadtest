module TableLoadTest

using Random
using DataFrames
using CSV
using SparseArrays
using Dates


"""
    nfeatures: total number of possible features
    nobservations: total number of observations
    nfeatperobs: Number of features in each observation
"""
function generate_testdata(nfeatures, nobservations, nfeatperobs, datadir=normpath(joinpath(@__DIR__, "..", "data")); force=false)
    if isdir(datadir)
        force ? rm(datadir, recursive=true) : error("$datadir exists, use `force=true` to overwrite")
    end
    mkdir(datadir)

    allfeatures = [randstring() for _ in 1:nfeatures]

    for o in 1:nobservations
        fpick = unique(rand(1:nfeatures, nfeatperobs))
        df = DataFrame(feature=allfeatures[fpick], value=rand(length(fpick)))
        CSV.write(joinpath(datadir, "obs$o.csv"), df)
    end
end

"""
Build dictionaries for indices, construct sparse matrix at the end
"""
function join_from_csv1(datadir=normpath(joinpath(@__DIR__, "..", "data")))
    loadStart = now()
    obs = map(f-> replace(f, ".csv"=>""), readdir(datadir))
    @info "Load Time: $(now() - loadStart)"

    obsStart = now()
    obsdict = Dict{String, Dict{String,Float64}}()
    features = Set()
    for ob in obs
        df = CSV.File(joinpath(datadir, "$ob.csv")) |> DataFrame
        obsdict[ob] = Dict(row.feature=> row.value for row in eachrow(df))
        union!(features, df.feature)
    end
    @info "Obs Time: $(now() - obsStart)"

    sortStart = now()
    features=sort(collect(features))
    @info "Sort Time: $(now() - sortStart)"

    featureStart = now()
    sa = spzeros(length(features),length(obs))
    featuredict = Dict(f=>i for (i,f) in enumerate(features))
    @info "Feature Time: $(now() - featureStart)"

    saStart = now()
    for (i, ob) in enumerate(obs)
        for f in keys(obsdict[ob])
            sa[featuredict[f], i] = obsdict[ob][f]
        end
    end
    @info "SA Time: $(now() - saStart)"

    return sa, features, obs
end

function join_from_csv2(datadir=normpath(joinpath(@__DIR__, "..", "data")))
    loadStart = now()
    obs = map(f-> replace(f, ".csv"=>""), readdir(datadir))
    @info "Load Time: $(now() - loadStart)"

    obsStart = now()
    obsdict = Dict{String, Dict{String,Float64}}()
    features = Set()
    for ob in obs
        df = CSV.File(joinpath(datadir, "$ob.csv")) |> DataFrame
        obsdict[ob] = Dict(row.feature=> row.value for row in eachrow(df))
        union!(features, df.feature)
    end
    @info "Obs Time: $(now() - obsStart)"

    sortStart = now()
    features=sort(collect(features))
    @info "Sort Time: $(now() - sortStart)"

    featureStart = now()
    arr = zeros(length(features),length(obs))
    featuredict = Dict(f=>i for (i,f) in enumerate(features))
    @info "Feature Time: $(now() - featureStart)"

    arrStart = now()
    for (i, ob) in enumerate(obs)
        for f in keys(obsdict[ob])
            arr[featuredict[f], i] = obsdict[ob][f]
        end
    end
    @info "Array Time: $(now() - arrStart)"

    saStart = now()
    sa = sparse(arr)
    @info "Sparse Time: $(now() - saStart)"

    return sa, features, obs
end

function join_from_csv3(datadir=normpath(joinpath(@__DIR__, "..", "data")))
    loadStart = now()
    obs = map(f-> replace(f, ".csv"=>""), readdir(datadir))
    @info "Load Time: $(now() - loadStart)"

    obsStart = now()
    obsdict = Dict{String, Dict{String,Float64}}()
    features = Set()
    for ob in obs
        df = CSV.File(joinpath(datadir, "$ob.csv")) |> DataFrame
        obsdict[ob] = Dict(row.feature=> row.value for row in eachrow(df))
        union!(features, df.feature)
    end
    @info "Obs Time: $(now() - obsStart)"

    sortStart = now()
    features=sort(collect(features))
    @info "Sort Time: $(now() - sortStart)"

    featureStart = now()
    arr = zeros(length(features),length(obs))
    featuredict = Dict(f=>i for (i,f) in enumerate(features))
    @info "Feature Time: $(now() - featureStart)"

    vecsStart = now()
    for (i, ob) in enumerate(obs)
        for f in keys(obsdict[ob])
            arr[featuredict[f], i] = obsdict[ob][f]
        end
    end
    @info "Vec Time: $(now() - arrStart)"

    saStart = now()
    sa = sparse(arr)
    @info "Sparse Time: $(now() - saStart)"

    return sa, features, obs
end

end # module
